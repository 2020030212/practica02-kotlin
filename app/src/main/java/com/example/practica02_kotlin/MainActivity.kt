package com.example.practica02_kotlin

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val txtAltura = findViewById<EditText>(R.id.txtAltura) as EditText;
        val txtPeso=findViewById<EditText>(R.id.txtPeso) as EditText;
        val lblImc=findViewById<TextView>(R.id.lblIMC) as TextView;
        val btnCalcular=findViewById<Button>(R.id.btnCalcular) as Button;
        val btnSalir=findViewById<Button>(R.id.btnSalir) as Button;
        val btnLimpiar=findViewById<Button>(R.id.btnLimpiar) as Button;


        btnLimpiar.setOnClickListener(View.OnClickListener {
            txtPeso.setText("");
            txtAltura.setText("");
            lblImc.setText("");
        })


        btnCalcular.setOnClickListener(View.OnClickListener {
            if(txtAltura.text.toString().equals("") || txtPeso.text.toString().equals(""))
            {
                Toast.makeText(this@MainActivity,"Hay campos vacios", Toast.LENGTH_SHORT).show();
            }
            else{

                var num1=txtAltura.text.toString().toDouble();
                var num2=txtPeso.text.toString().toDouble();
                var res=num2/(num1*num1);

                //val formato = DecimalFormat("#.##")

                val IMC = (Math.round(res * 10.0) / 10.0)
                lblImc.setText("El IMC es: $IMC")

            }
        });

        btnSalir.setOnClickListener(View.OnClickListener {
            finish();
        })

    }
}